from typing import Any, Optional

from humps import camelize
from pydantic import BaseModel


def to_upper_case(value: str) -> str:
    return value.upper()


def to_lower_case(value: str) -> str:
    return value.lower()


def to_camel_case(value: str) -> str:
    return camelize(value)


class BaseSchema(BaseModel):

    class Config:
        alias_generator = to_upper_case
        allow_population_by_field_name = True


class ResultSchema(BaseSchema):
    status: int
    error_msg: str
    date: str


class ParameterSchema(BaseSchema):
    lang: str
    data_format: str
    survey_years: Optional[str]
    open_years: Optional[str]
    stats_field: Optional[str]
    stats_code: Optional[str]
    small_area: Optional[str]
    search_word: Optional[str]
    search_kind: Optional[str]
    collect_area: Optional[str]
    explanation_get_flg: Optional[str]
    stats_name_list: Optional[str]
    start_position: Optional[str]
    limit: Optional[str]
    updated_date: Optional[str]
    callback: Optional[Any]
