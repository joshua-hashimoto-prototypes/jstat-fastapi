from typing import List

from pydantic import BaseModel, Field

from app.schemas import commons as common_schema


# response from e-Stat api

class BaseSchema(BaseModel):

    class Config:
        alias_generator = common_schema.to_upper_case
        allow_population_by_field_name = True


class ResultInfoSchema(BaseSchema):
    from_number: int
    to_number: int


class ListInfoItemSchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class ListInfoSchema(BaseSchema):
    id: str
    stat_name: ListInfoItemSchema
    gov_org: ListInfoItemSchema

    def __init__(self, **data) -> None:
        id = data.get('@id')
        return super().__init__(id=id, **data)


class DataListInfoSchema(BaseSchema):
    number: int
    result_inf: ResultInfoSchema
    list_inf: List[ListInfoSchema]


class StatsNameListSchema(BaseSchema):
    result: common_schema.ResultSchema
    parameter: common_schema.ParameterSchema
    datalist_inf: DataListInfoSchema


class RootSchema(BaseSchema):
    get_stats_list: StatsNameListSchema

    def get_root(self) -> StatsNameListSchema:
        return self.get_stats_list


# response to api caller

class ResponseBaseSchema(BaseModel):

    class Config:
        alias_generator = common_schema.to_camel_case
        allow_population_by_field_name = True


class ResponseDataRangeSchema(ResponseBaseSchema):
    from_number: int
    to_number: int

    class Config:
        schema_extra = {
            'example': {
                'from_number': 0,
                'to_number': 100,
            }
        }


class ResponseOrganizationSchema(ResponseBaseSchema):
    code: str
    name: str


class ResponseListInfoSchema(ResponseBaseSchema):
    id: str
    research_name: str
    organization: ResponseOrganizationSchema

    class Config:
        schema_extra = {
            'example': {
                'id': '00000000',
                'research_name': 'The name of the research.',
                'organization': {
                    'code': '00000',
                    'name': 'the name of the organization that conducted the research',
                },
            }
        }


class ResponseSchema(ResponseBaseSchema):
    estat_status: int
    total: int
    data_range: ResponseDataRangeSchema
    result: List[ResponseListInfoSchema]

    class Config:
        schema_extra = {
            'example': {
                'estat_status': 0,
                'total': 2,
                'data_range': {
                    'from_number': 0,
                    'to_number': 2,
                },
                'result': [
                    {
                        'id': '00000000',
                        'research_name': 'The name of the research.',
                        'organization': 'The organization that did the research.',
                    },
                    {
                        'id': '00000000',
                        'research_name': 'The name of the research.',
                        'organization': 'The organization that did the research.',
                    },
                ],
            }
        }


# utility functions


def get_list_info(schema: ResponseListInfoSchema) -> ResponseListInfoSchema:
    list_info = ResponseListInfoSchema(
        id=schema.id,
        research_name=schema.stat_name.description,
        organization={'code': schema.gov_org.code,
                      'name': schema.gov_org.description}
    )
    return list_info


def get_response_schema(schema: RootSchema) -> ResponseSchema:
    datalist = schema.get_stats_list.datalist_inf
    context = {
        'estat_status': schema.get_stats_list.result.status,
        'total': datalist.number,
        'data_range': datalist.result_inf,
        'result': [get_list_info(x) for x in datalist.list_inf],
    }
    return ResponseSchema(**context)
