from typing import List, Union, Optional

from pydantic import BaseModel

from app.schemas import commons as common_schema


# response from e-Stat api

class BaseSchema(BaseModel):

    class Config:
        alias_generator = common_schema.to_upper_case
        allow_population_by_field_name = True


class ResultInfoSchema(BaseSchema):
    from_number: int
    to_number: int


class StatNameSchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class GovOrgSchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class TitleSchema(BaseSchema):
    no: str
    description: str

    def __init__(self, **data) -> None:
        no = data.get('@no')
        description = data.get('$')
        return super().__init__(no=no, description=description, **data)


class MainCategorySchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class SubCategorySchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class StatisticsNameSpecSchema(BaseSchema):
    tabulation_category: str
    tabulation_sub_category1: Optional[str] = None
    tabulation_sub_category2: Optional[str] = None
    tabulation_sub_category3: Optional[str] = None
    tabulation_sub_category4: Optional[str] = None
    tabulation_sub_category5: Optional[str] = None


class TitleSpecSchema(BaseSchema):
    table_category: Optional[str] = None
    table_name: Optional[str] = None
    table_explanation: Optional[str] = None


class TableInfoSchema(BaseSchema):
    id: str
    stat_name: StatNameSchema
    gov_org: GovOrgSchema
    statistics_name: str
    title: Optional[Union[TitleSchema, str]]
    cycle: str
    survey_date: str
    open_date: str
    small_area: int
    collect_area: str
    main_category: MainCategorySchema
    sub_category: SubCategorySchema
    overall_total_number: int
    updated_date: str
    statistics_name_spec: StatisticsNameSpecSchema
    description: Optional[str] = None
    title_spec: TitleSpecSchema

    def __init__(self, **data) -> None:
        id = data.get('@id')
        return super().__init__(id=id, **data)


class DataListInfoSchema(BaseSchema):
    number: int
    result_inf: Union[ResultInfoSchema, str]
    table_inf: Union[List[TableInfoSchema], TableInfoSchema, None]


class StatsNameListSchema(BaseSchema):
    result: common_schema.ResultSchema
    parameter: common_schema.ParameterSchema
    datalist_inf: DataListInfoSchema


class RootSchema(BaseSchema):
    get_stats_list: StatsNameListSchema

    def get_root(self) -> StatsNameListSchema:
        return self.get_stats_list


# response to api caller

class ResponseBaseSchema(BaseModel):

    class Config:
        alias_generator = common_schema.to_camel_case
        allow_population_by_field_name = True


class ResponseDataRangeSchema(ResponseBaseSchema):
    from_number: int
    to_number: int

    class Config:
        schema_extra = {
            'example': {
                'from_number': 0,
                'to_number': 100,
            }
        }


class ResponseOrganizationSchema(ResponseBaseSchema):
    code: str
    name: str


class ResponseCategorySchema(ResponseBaseSchema):
    code: str
    name: str


class ResponseTableInfoSchema(ResponseBaseSchema):
    id: str
    statistics_name: str
    organization: ResponseOrganizationSchema
    category: ResponseCategorySchema
    sub_category: ResponseCategorySchema
    collect_area: str
    publish_date: str
    updated_date: str
    overall_total: int
    description: Optional[str] = None

    class Config:
        schema_extra = {
            'example': {
                'id': '00000000',
                'statistics_name': '統計表の提供統計名及び提供分類名',
                'organization': {
                    'code': '00000',
                    'name': 'the name of the organization that conducted the research',
                },
                'category': {
                    'code': '00',
                    'name': 'category name'
                },
                'sub_category': {
                    'code': '00',
                    'name': 'category name'
                },
                'collect_area': 'area name',
                'publish_date': '',
                'updated_date': '',
                'overall_total': 1000,
                'description': 'description. this could be null',
            }
        }


class ResponseSchema(ResponseBaseSchema):
    estat_status: int
    total: int
    data_range: ResponseDataRangeSchema
    result: Optional[List[ResponseTableInfoSchema]] = None

    class Config:
        schema_extra = {
            'example': {
                'estat_status': 0,
                'total': 2,
                'data_range': {
                    'from_number': 0,
                    'to_number': 2,
                },
                'result': [
                    {
                        'id': '00000000',
                        'statistics_name': '統計表の提供統計名及び提供分類名',
                        'organization': {
                            'code': '00000',
                            'name': 'the name of the organization that conducted the research',
                        },
                        'category': {
                            'code': '00',
                            'name': 'category name'
                        },
                        'sub_category': {
                            'code': '00',
                            'name': 'category name'
                        },
                        'collect_area': 'area name',
                        'publish_date': '',
                        'updated_date': '',
                        'overall_total': 1000,
                        'description': 'description. this could be null',
                    },
                    {
                        'id': '00000000',
                        'statistics_name': '統計表の提供統計名及び提供分類名',
                        'organization': {
                            'code': '00000',
                            'name': 'the name of the organization that conducted the research',
                        },
                        'category': {
                            'code': '00',
                            'name': 'category name'
                        },
                        'sub_category': {
                            'code': '00',
                            'name': 'category name'
                        },
                        'collect_area': 'area name',
                        'publish_date': '',
                        'updated_date': '',
                        'overall_total': 1000,
                        'description': 'description. this could be null',
                    },
                ],
            }
        }


# utility functions

def get_table_info(schema: ResponseTableInfoSchema) -> ResponseTableInfoSchema:
    table_info = ResponseTableInfoSchema(
        id=schema.id,
        statistics_name=schema.statistics_name,
        organization={'code': schema.gov_org.code,
                      'name': schema.gov_org.description},
        category={'code': schema.main_category.code,
                  'name': schema.main_category.description},
        sub_category={'code': schema.sub_category.code,
                      'name': schema.sub_category.description},
        collect_area=schema.collect_area,
        publish_date=schema.open_date,
        updated_date=schema.updated_date,
        overall_total=schema.overall_total_number,
        description=schema.description,
    )
    return table_info


def get_response_schema(schema: RootSchema) -> ResponseSchema:
    table_info_list = schema.get_stats_list.datalist_inf.table_inf
    if isinstance(table_info_list, TableInfoSchema):
        table_info_list = [get_table_info(table_info_list)]
    elif table_info_list is None:
        table_info_list = []
    else:
        table_info_list = [get_table_info(x) for x in table_info_list]

    context = {
        'estat_status': schema.get_stats_list.result.status,
        'total': schema.get_stats_list.datalist_inf.number,
        'data_range': schema.get_stats_list.datalist_inf.result_inf,
        'result': table_info_list
    }
    return ResponseSchema(**context)
