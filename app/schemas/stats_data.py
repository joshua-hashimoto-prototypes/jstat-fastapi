from typing import List, Union, Optional

from pydantic import BaseModel, Field

from app.schemas import commons as common_schema


# response from e-Stat api

class BaseSchema(BaseModel):

    class Config:
        alias_generator = common_schema.to_upper_case
        allow_population_by_field_name = True


class ResultInfoSchema(BaseSchema):
    total_number: int
    from_number: int
    to_number: int
    next_key: Optional[int] = None


class StatNameSchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class GovOrgSchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class TitleSchema(BaseSchema):
    no: str
    description: str

    def __init__(self, **data) -> None:
        no = data.get('@no')
        description = data.get('$')
        return super().__init__(no=no, description=description, **data)


class MainCategorySchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class SubCategorySchema(BaseSchema):
    code: str
    description: str

    def __init__(self, **data) -> None:
        code = data.get('@code')
        description = data.get('$')
        return super().__init__(code=code, description=description, **data)


class StatisticsNameSpecSchema(BaseSchema):
    tabulation_category: str
    tabulation_sub_category1: Optional[str] = None
    tabulation_sub_category2: Optional[str] = None
    tabulation_sub_category3: Optional[str] = None
    tabulation_sub_category4: Optional[str] = None
    tabulation_sub_category5: Optional[str] = None


class TitleSpecSchema(BaseSchema):
    table_category: Optional[str] = None
    table_name: Optional[str] = None
    table_explanation: Optional[str] = None


class DescriptionSchema(BaseSchema):
    tabulation_sub_category_explanation1: Optional[str] = None
    tabulation_sub_category_explanation2: Optional[str] = None
    tabulation_sub_category_explanation3: Optional[str] = None
    tabulation_sub_category_explanation4: Optional[str] = None
    tabulation_sub_category_explanation5: Optional[str] = None


class TableInfoSchema(BaseSchema):
    id: str
    stat_name: StatNameSchema
    gov_org: GovOrgSchema
    statistics_name: str
    title: Optional[Union[TitleSchema, str]] = None
    cycle: str
    survey_date: str
    open_date: str
    small_area: int
    collect_area: str
    main_category: MainCategorySchema
    sub_category: SubCategorySchema
    overall_total_number: int
    updated_date: str
    statistics_name_spec: StatisticsNameSpecSchema
    description: Optional[Union[str, DescriptionSchema]] = None
    title_spec: TitleSpecSchema

    def __init__(self, **data) -> None:
        id = data.get('@id')
        return super().__init__(id=id, **data)


class ClassObjClassItemSchema(BaseSchema):
    code: Optional[str] = None
    name: Optional[str] = None
    level: Optional[str] = None
    unit: Optional[str] = None
    parent_code: Optional[str] = None

    def __init__(self, **data):
        code = data.get('@code')
        name = data.get('@name')
        level = data.get('@level')
        unit = data.get('@unit')
        parent_code = data.get('@parentCode')
        return super().__init__(code=code, name=name, level=level, unit=unit, parent_code=parent_code, **data)


class ExplanationItemSchema(BaseSchema):
    code: str
    name: str

    def __init__(self, **data) -> None:
        code = data.get('@id')
        name = data.get('$')
        return super().__init__(code=code, name=name)


class ClassObjItemSchema(BaseSchema):
    id: str
    name: str
    classes: Union[List[ClassObjClassItemSchema],
                   ClassObjClassItemSchema] = Field(..., alias='CLASS')
    explanations: Optional[Union[List[ExplanationItemSchema],
                                 ExplanationItemSchema]] = Field(None, alias='EXPLANATION')

    def __init__(self, **data) -> None:
        id = data.get('@id')
        name = data.get('@name')
        return super().__init__(id=id, name=name, **data)


class ClassInfoSchem(BaseSchema):
    class_obj: List[ClassObjItemSchema]


class NoteItemSchema(BaseSchema):
    char: str
    description: str

    def __init__(self, **data) -> None:
        char = data.get('@char')
        description = data.get('$')
        return super().__init__(char=char, description=description, **data)


class ValueItemSchema(BaseSchema):
    tab: Optional[str] = None
    cat01: Optional[str] = None
    cat02: Optional[str] = None
    cat03: Optional[str] = None
    cat04: Optional[str] = None
    cat05: Optional[str] = None
    area: Optional[str] = None
    time: Optional[str] = None
    unit: Optional[str] = None
    total: Optional[str] = None

    def __init__(self, **data) -> None:
        tab = data.get('@tab')
        cat01 = data.get('@cat01')
        cat02 = data.get('@cat02')
        cat03 = data.get('@cat03')
        cat04 = data.get('@cat04')
        cat05 = data.get('@cat05')
        area = data.get('@area')
        time = data.get('@time')
        unit = data.get('@unit')
        total = data.get('$')
        return super().__init__(tab=tab, cat01=cat01, cat02=cat02, cat03=cat03, cat04=cat04, cat05=cat05, area=area, time=time, unit=unit, total=total, **data)


class DataInfoSchema(BaseSchema):
    note: Union[List[NoteItemSchema], NoteItemSchema]
    value: Union[List[ValueItemSchema], ValueItemSchema]


class StatisticalDataSchema(BaseSchema):
    result_inf: ResultInfoSchema
    table_inf: TableInfoSchema
    class_inf: ClassInfoSchem
    data_inf: DataInfoSchema


class StatsDataSchema(BaseSchema):
    result: common_schema.ResultSchema
    parameter: common_schema.ParameterSchema
    statistical_data: Optional[StatisticalDataSchema] = None


class RootSchema(BaseSchema):
    get_stats_data: StatsDataSchema

    def get_root(self) -> StatsDataSchema:
        return self.get_stats_data


# response to api caller

class ResponseBaseSchema(BaseModel):

    class Config:
        alias_generator = common_schema.to_camel_case
        allow_population_by_field_name = True


class ResponseItemSchema(ResponseBaseSchema):
    pass


class ResponseHeaderItemSchema(ResponseBaseSchema):
    key: str
    value: str


class ResponseStatNameSchema(ResponseBaseSchema):
    code: str
    description: str


class ResponseGovOrgSchema(ResponseBaseSchema):
    code: str
    description: str


class ResponseTitleSchema(ResponseBaseSchema):
    no: str
    description: str


class ResponseMainCategorySchema(ResponseBaseSchema):
    code: str
    description: str


class ResponseSubCategorySchema(ResponseBaseSchema):
    code: str
    description: str


class ResponseStatisticsNameSpecSchema(ResponseBaseSchema):
    tabulation_category: str
    tabulation_sub_category1: Optional[str] = None
    tabulation_sub_category2: Optional[str] = None
    tabulation_sub_category3: Optional[str] = None
    tabulation_sub_category4: Optional[str] = None
    tabulation_sub_category5: Optional[str] = None


class ResponseTitleSpecSchema(ResponseBaseSchema):
    table_category: Optional[str] = None
    table_name: Optional[str] = None
    table_explanation: Optional[str] = None


class ResponseDescriptionSchema(ResponseBaseSchema):
    tabulation_sub_category_explanation1: Optional[str] = None
    tabulation_sub_category_explanation2: Optional[str] = None
    tabulation_sub_category_explanation3: Optional[str] = None
    tabulation_sub_category_explanation4: Optional[str] = None
    tabulation_sub_category_explanation5: Optional[str] = None


class ResponseMetaDataSchema(ResponseBaseSchema):
    id: str
    stat_name: ResponseStatNameSchema
    gov_org: ResponseGovOrgSchema
    statistics_name: str
    title: Optional[Union[ResponseTitleSchema, str]] = None
    cycle: str
    survey_date: str
    open_date: str
    small_area: int
    collect_area: str
    main_category: ResponseMainCategorySchema
    sub_category: ResponseSubCategorySchema
    overall_total_number: int
    updated_date: str
    statistics_name_spec: ResponseStatisticsNameSpecSchema
    description: Optional[Union[str, ResponseDescriptionSchema]] = None
    title_spec: ResponseTitleSpecSchema


class ResponseDatasetDetailSchema(ResponseBaseSchema):
    id: Optional[str] = None
    value: Optional[str] = None


class ResponseDatasetItemSchema(ResponseBaseSchema):
    tab: Optional[ResponseDatasetDetailSchema] = None
    cat01: Optional[ResponseDatasetDetailSchema] = None
    cat02: Optional[ResponseDatasetDetailSchema] = None
    cat03: Optional[ResponseDatasetDetailSchema] = None
    cat04: Optional[ResponseDatasetDetailSchema] = None
    cat05: Optional[ResponseDatasetDetailSchema] = None
    area: Optional[ResponseDatasetDetailSchema] = None
    time: Optional[ResponseDatasetDetailSchema] = None
    unit: Optional[Union[ResponseDatasetDetailSchema, str]] = None
    total: Union[int, str] = ''


class ResponseSchema(ResponseBaseSchema):
    headers: List[ResponseHeaderItemSchema]
    metadata: ResponseMetaDataSchema
    datasets: List[ResponseDatasetItemSchema]

    class Config:
        schema_extra = {
            'example': {
                'header': [
                    {
                        'key': 'key that co-responses to the dataset items key',
                        'value': 'the actual value of the header item',
                    },
                    {
                        "key": "tab",
                        "value": "表章項目"
                    },
                ],
                'metadata': {
                    "id": "0003334000",
                    "statName": {
                        "code": "00350300",
                        "description": "普通貿易統計"
                    },
                    "govOrg": {
                        "code": "00350",
                        "description": "財務省"
                    },
                    "statisticsName": "貿易統計_税関別 税関別品別国別表 輸出",
                    "title": "確報 税関別品別国別表 (輸出 1-12月：確定)　2019年；　(輸出 1-8月：確報)2020年",
                    "cycle": "年次",
                    "surveyDate": "201901-201912",
                    "openDate": "2020-09-29",
                    "smallArea": 0,
                    "collectArea": "該当なし",
                    "mainCategory": {
                        "code": "16",
                        "description": "国際"
                    },
                    "subCategory": {
                        "code": "01",
                        "description": "貿易・国際収支"
                    },
                    "overallTotalNumber": 34275780,
                    "updatedDate": "2020-09-29",
                    "statisticsNameSpec": {
                        "tabulationCategory": "貿易統計_税関別",
                        "tabulationSubCategory1": "税関別品別国別表",
                        "tabulationSubCategory2": "輸出",
                        "tabulationSubCategory3": 'null',
                        "tabulationSubCategory4": 'null',
                        "tabulationSubCategory5": 'null'
                    },
                    "description": {
                        "tabulationSubCategoryExplanation1": 'null',
                        "tabulationSubCategoryExplanation2": "1月～表示月分までのデータ掲載です",
                        "tabulationSubCategoryExplanation3": 'null',
                        "tabulationSubCategoryExplanation4": 'null',
                        "tabulationSubCategoryExplanation5": 'null'
                    },
                    "titleSpec": {
                        "tableCategory": "確報",
                        "tableName": "税関別品別国別表 (輸出 1-12月：確定)　2019年；　(輸出 1-8月：確報)2020年",
                        "tableExplanation": 'null'
                    }
                },
                'datasets': [
                    {
                        'tab': 'the tab name of the data',
                        'cat01': 'category 1 of the data',
                        'cat02': 'category 2 of the data',
                        'cat03': 'category 3 of the data',
                        'cat04': 'category 4 of the data',
                        'cat05': 'category 5 of the data',
                        'area': 'the area the research was conducted',
                        'time': 'the time representation when the research was conducted. All in Japanese Era',
                        'unit': 'the unit of the total',
                        'total': 'total value of the data',
                    },
                    {
                        "tab": "人口",
                        "cat01": "男女計",
                        "cat02": "総数",
                        "cat03": "総人口",
                        "cat04": "総人口",
                        "cat05": "総人口",
                        "area": "全国",
                        "time": "平成30年10月1日現在",
                        "unit": "千人",
                        "total": "126443"
                    },
                ],
            }
        }


# def get_response_schema(schema: RootSchema) -> ResponseSchema:
def get_response_schema(schema: RootSchema):
    response = {}
    statistical_data = schema.get_stats_data.statistical_data
    class_obj = statistical_data.class_inf.class_obj
    header_list = [{'key': x.id, 'value': x.name} for x in class_obj]

    response['headers'] = header_list
    response['metadata'] = statistical_data.table_inf
    response['datasets'] = []

    for value in statistical_data.data_inf.value:
        value_dict = value.dict()
        for class_obj_item in class_obj:
            if class_obj_item.explanations is not None and isinstance(class_obj_item.explanations, list):
                for explanation in class_obj_item.explanations:
                    if value_dict[class_obj_item.id] == explanation.code:
                        value_dict[class_obj_item.id] = {
                            'id': explanation.code, 'value': explanation.name}
            else:
                if isinstance(class_obj_item.classes, ClassObjClassItemSchema):
                    class_item = class_obj_item.classes
                    if value_dict[class_obj_item.id] == class_item.code:
                        value_dict[class_obj_item.id] = {
                            'id': class_item.code, 'value': class_item.name}
                if isinstance(class_obj_item.classes, list):
                    for class_item in class_obj_item.classes:
                        if value_dict[class_obj_item.id] == class_item.code:
                            value_dict[class_obj_item.id] = {
                                'id': class_item.code, 'value': class_item.name}
        response['datasets'].append(value_dict)

    # TODO: add total data for every property to response
    # return response
    return ResponseSchema(**response)
