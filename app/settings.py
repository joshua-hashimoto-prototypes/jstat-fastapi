from pydantic import BaseSettings


class Settings(BaseSettings):
    estat_appid: str

    class Config:
        env_file = '.env'
