from typing import Optional
from fastapi import FastAPI, APIRouter, Path, Depends, Query
from pydantic.tools import parse_obj_as

from app.dependencies import EstatAPIDependency
from app.schemas import stats_name_list as snl_schema
from app.schemas import stats_list as sl_schema
from app.schemas import stats_data as sd_schema


router = APIRouter()


def set_estat_router(app: FastAPI):
    app.include_router(
        router,
        prefix='/api/e-stat',
        tags=['e-stat']
    )


@router.get(
    '/statsCodeList/',
    summary="政府統計コード一覧取得",
    response_description="e-Statから政府統計コードの一覧を取得",
    response_model=snl_schema.ResponseSchema,
    response_model_exclude_none=True,
)
async def fetch_estat_stasts_code_name_list(
    search_word: Optional[str] = Query(None, alias='searchWord'),
    limit: Optional[int] = Query(None),
    estat: EstatAPIDependency = Depends(),
):
    """
    # e-Statの政府統計コードの一覧を取得
    """
    # set dependency data
    estat.api_data_source = 'getStatsList'
    estat.query_params = {'statsNameList': 'Y'}

    if search_word is not None:
        estat.query_params.update({'searchWord': search_word})

    if limit is not None:
        estat.query_params.update({'limit': limit})

    # fetch data from e-Stat api
    response = estat.get_request(snl_schema.RootSchema)
    # create response schema
    parsed_data = snl_schema.get_response_schema(response)
    return parsed_data


@router.get(
    '/statsCode/{stats_code}/',
    summary='政府統計コードから統計表情報を取得',
    response_description="e-Statから政府統計コードを元に統計表情報を取得",
    response_model=sl_schema.ResponseSchema,
    response_model_exclude_none=True,
)
async def fetch_estat_stats_code_list(
    stats_code: str = Path(...),
    search_word: Optional[str] = Query(None, alias='searchWord'),
    limit: Optional[int] = Query(None),
    estat: EstatAPIDependency = Depends(),
):
    """
    # e-Statの統計表情報を取得

    この情報の取得には政府統計コードが必要
    """
    # set dependency data
    estat.api_data_source = 'getStatsList'
    estat.query_params = {'statsCode': stats_code}

    if search_word is not None:
        estat.query_params.update({'searchWord': search_word})

    if limit is not None:
        estat.query_params.update({'limit': limit})

    # fetch data from e-Stat api
    response = estat.get_request(sl_schema.RootSchema)
    # create response schema
    parsed_data = sl_schema.get_response_schema(response)
    return parsed_data


@router.get(
    '/statsData/{stats_data_id}/',
    summary='統計表IDから統計情報を取得',
    response_description='e-Statから統計表IDを元に統計情報を取得',
    response_model=sd_schema.ResponseSchema,
    response_model_exclude_none=True,
)
async def fetch_estat_stats_data(
    stats_data_id: str = Path(..., title='getStatsListから取得できるStatsDataId'),
    page: Optional[int] = Query(None),
    limit: int = Query(1000),
    estat: EstatAPIDependency = Depends(),
):
    """
    # e-Statの統計データ情報を取得

    この情報の取得には情報表IDが必要
    """
    # set dependency data
    estat.api_data_source = 'getStatsData'
    estat.query_params = {'statsDataId': stats_data_id}
    start_position = 1
    if page is None or page == 1:
        estat.query_params.update(
            {'startPosition': start_position, 'limit': limit})
    else:
        estat.query_params.update(
            {'startPosition': start_position + (page * limit), 'limit': limit})

    # fetch data from e-Stat api
    response = estat.get_request(sd_schema.RootSchema)
    # create response schema
    parsed_data = sd_schema.get_response_schema(response)
    return parsed_data
