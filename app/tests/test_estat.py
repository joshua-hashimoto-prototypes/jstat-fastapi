from typing import Optional, Tuple

from fastapi.testclient import TestClient
from fastapi.responses import Response


from app.main import app


client = TestClient(app)


def call_stats_name_list() -> Tuple[Response, dict]:
    response = client.get('/api/e-stat/statsCodeList/')
    json = response.json()
    return (response, json)


def call_stats_list(stats_code: Optional[str] = None) -> Tuple[Response, dict]:
    res, json = call_stats_name_list()
    if stats_code is None:
        stats_code_id = json.get('result')[0].get('id')
    else:
        stats_code_id = stats_code
    response = client.get(f'/api/e-stat/statsCode/{stats_code_id}/')
    json = response.json()
    return (response, json)


def test_stats_name_list():
    response, json = call_stats_name_list()
    assert response.status_code == 200
    assert 'estatStatus' in json
    assert 'total' in json
    assert 'dataRange' in json
    assert 'result' in json


def test_stats_list():
    response, json = call_stats_list()
    assert response.status_code == 200
    assert 'estatStatus' in json
    assert 'total' in json
    assert 'dataRange' in json
    assert 'result' in json


def test_fail_stats_list():
    response = client.get(f'/api/e-stat/statsCode/00000000/')
    json = response.json()
    assert response.status_code == 400
    assert json.get('estatStatus') == 1
    assert json.get('message') == 'No data.'


def test_stats_data():
    response, json = call_stats_list()
    stats_data_id = json.get('result')[0].get('id')
    response = client.get(f'/api/e-stat/statsData/{stats_data_id}/')
    json = response.json()
    assert response.status_code == 200
    assert 'headers' in json
    assert 'metadata' in json
    assert 'datasets' in json


def test_fail_stats_data():
    response, json = call_stats_list()
    response = client.get(f'/api/e-stat/statsData/0000000000/')
    json = response.json()
    assert response.status_code == 400
    assert json.get('estatStatus') == 300
    assert json.get('message') == 'No data.'
