class EstatBaseException(Exception):

    def __init__(self, estat_status, estat_error_message) -> None:
        self.estat_status = estat_status
        self.estat_error_message = estat_error_message


class EstatRequestException(EstatBaseException):
    pass


class EstatDatabaseException(EstatBaseException):
    pass


class EstatNoDataException(EstatBaseException):
    pass
