from fastapi import Request
from fastapi.responses import JSONResponse

from .exception_classes import (
    EstatRequestException, EstatDatabaseException, EstatNoDataException,)


def estat_request_exception_handler(_: Request, exc: EstatRequestException):
    status_code = 400
    context = {
        'statusCode': status_code,
        'message': 'Request failed.',
        'estatStatus': exc.estat_status,
        'estatMessage': exc.estat_error_message,
    }
    return JSONResponse(status_code=status_code, content=context)


def estat_database_exception_handler(_: Request, exc: EstatDatabaseException):
    status_code = 500
    context = {
        'statusCode': status_code,
        'message': 'Database has failed.',
        'estatStatus': exc.estat_status,
        'estatMessage': exc.estat_error_message,
    }
    return JSONResponse(status_code=status_code, content=context)


def estat_no_data_exception_handler(_: Request, exc: EstatNoDataException):
    status_code = 400
    context = {
        'statusCode': status_code,
        'message': 'No data.',
        'estatStatus': exc.estat_status,
        'estatMessage': exc.estat_error_message,
    }
    return JSONResponse(status_code=status_code, content=context)
