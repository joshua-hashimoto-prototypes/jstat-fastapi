from fastapi import FastAPI

from .exception_classes import (
    EstatRequestException, EstatDatabaseException, EstatNoDataException,)
from .exception_handlers import (estat_request_exception_handler,
                                 estat_database_exception_handler, estat_no_data_exception_handler,)


def set_exception_hander(app: FastAPI):
    app.add_exception_handler(
        EstatRequestException,
        estat_request_exception_handler,
    )
    app.add_exception_handler(
        EstatDatabaseException,
        estat_database_exception_handler,
    )
    app.add_exception_handler(
        EstatNoDataException,
        estat_no_data_exception_handler,
    )
