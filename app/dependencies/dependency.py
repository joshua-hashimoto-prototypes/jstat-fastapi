from functools import lru_cache
from typing import Dict, Any, Type, TypeVar

from fastapi import status
from pydantic import parse_obj_as
import requests

from app.settings import Settings
from app.exceptions import (EstatRequestException,
                            EstatDatabaseException, EstatNoDataException,)


@lru_cache()
def get_settings():
    return Settings()


T = TypeVar('T')


class EstatAPIDependency:

    def __init__(self) -> None:
        self.__appid = get_settings().estat_appid
        self.__api_base_domain: str = 'https://api.e-stat.go.jp/rest'
        self.__api_version: float = 3.0
        self.__api_response_type: str = 'json'
        self.__api_response_language: str = 'J'
        self.__api_data_source: str = ''
        self.__api_request_headers: Dict[str, str] = {
            "content-type": "application/json"}
        self.__query_params: Dict[str, Any] = {}

    @property
    def api_data_source(self) -> str:
        return self.__api_data_source

    @api_data_source.setter
    def api_data_source(self, api_data_source: str):
        self.__api_data_source = api_data_source

    @property
    def api_request_headers(self) -> Dict[str, str]:
        return self.__api_request_headers

    @api_request_headers.setter
    def api_request_headers(self, api_request_headers: Dict[str, str]):
        self.__api_request_headers = api_request_headers

    @property
    def query_params(self) -> Dict[str, Any]:
        return self.__query_params

    @query_params.setter
    def query_params(self, query_params: Dict[str, Any]):
        self.__query_params = query_params

    def get_query_params(self):
        query_params_list = [f'{key}={value}' for key,
                             value in self.query_params.items()]
        return '&'.join(query_params_list)

    def get_url(self):
        domain = self.__api_base_domain
        version = self.__api_version
        response_type = self.__api_response_type
        data_source = self.__api_data_source
        appid = self.__appid
        language = self.__api_response_language

        path_param = f'{domain}/{version}/app/{response_type}/{data_source}'
        query_param = f'?appId={appid}&lang={language}&{self.get_query_params()}'
        # query_param = f'?appId={appid}&lang={language}&explanationGetFlg=N&{self.get_query_params()}'
        return path_param + query_param

    def get_request(self, parse_class: Type[T]) -> T:
        response = requests.get(
            self.get_url(), headers=self.api_request_headers)
        json = response.json()
        parsed_response = parse_obj_as(parse_class, json)

        if response.status_code is status.HTTP_400_BAD_REQUEST:
            raise EstatRequestException(
                estat_status=parsed_response.get_root().result.status,
                estat_error_message=parsed_response.get_root().result.error_msg)

        if response.status_code is status.HTTP_500_INTERNAL_SERVER_ERROR:
            raise EstatDatabaseException(
                estat_status=parsed_response.get_root().result.status,
                estat_error_message=parsed_response.get_root().result.error_msg)

        if response.status_code is status.HTTP_200_OK and parsed_response.get_root().result.status:
            raise EstatNoDataException(
                estat_status=parsed_response.get_root().result.status,
                estat_error_message=parsed_response.get_root().result.error_msg)

        return parsed_response
