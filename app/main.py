from fastapi import FastAPI

from .middlewares import set_middleware
from .exceptions import set_exception_hander
from .routers import set_estat_router

app = FastAPI(
    title='J-Stats API',
    description='API endpoints for j-stats application',
    version='0.1.0',
)


# configs
set_middleware(app)
set_exception_hander(app)
set_estat_router(app)


# uvicorn app.main:app --reload
